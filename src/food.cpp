#include <iostream>
#include "brainlife.pb.h"
#include "food.h"

Food::Food(int start_x, int start_y, int start_energy) {
    x = start_x;
    y = start_y;
    energy = start_energy;
}

int Food::eat(int amt) {
    int eaten = amt > energy ? energy : amt;
    energy -= eaten;
    return eaten;
}

void Food::add_energy(int e) {
    energy += e;
    if (energy > constants::FOOD_MAX) {
        energy = constants::FOOD_MAX;
    }
}

void Food::pb_serialize(brainlife::Food *pb_food) {
    pb_food->set_x(x);
    pb_food->set_y(y);
    pb_food->set_energy(energy);
}

Food& Food::operator=(const brainlife::Food &pb_food) {
    x = pb_food.x();
    y = pb_food.y();
    energy = pb_food.energy();
    return *this;
}

Food::Food(const brainlife::Food &pb_food) {
    *this = pb_food;
}

void Food::debug() {
    std::cout << "{x: " << x << ", y: " << y << ", e: " << energy << "}" << std::endl;
}
