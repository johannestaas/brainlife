#pragma once
#include <iostream>
#include <memory>
#include "brainlife.pb.h"
#include "constants.h"

class Food {
    public:
        Food(int start_x, int start_y, int start_energy);
        Food(const brainlife::Food&);
        Food() = default;
        ~Food() = default;
        Food(const Food&) = default;
        Food& operator=(const Food&) = default;
        Food& operator=(const brainlife::Food&);
        void add_energy(int e);
        Pos get_pos() const { return Pos({x, y}); };
        int get_energy() const { return energy; };
        int eat(int amt);
        void debug();
        bool is_gone() const { return energy == 0; };
        int x, y;
        int energy;
        void pb_serialize(brainlife::Food*);
};

typedef std::shared_ptr<Food> FoodP;
