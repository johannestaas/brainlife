#include <memory>
#include <iostream>
#include <sstream>
#include <string>
#include <signal.h>
#include <brain/genetic.h>
#include <brain/train.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/stat.h>

#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glext.h>

#include "math.h"
#include "constants.h"
#include "world.h"

extern char *optarg;
extern int optind, opterr, optopt;

unsigned int WINDOW_WIDTH = 1024, WINDOW_HEIGHT = 768;
const int SIZE = WINDOW_WIDTH * WINDOW_HEIGHT;
static bool CTRLC = false;

static World world(WINDOW_WIDTH, WINDOW_HEIGHT);
static unsigned long int i = 0;

static void func_sigint_handler(int s) {
    CTRLC = true;
    signal(SIGINT, SIG_DFL);
}

static void setup_sigint() {
    struct sigaction sigint_handler;
    sigint_handler.sa_handler = func_sigint_handler;
    sigemptyset(&sigint_handler.sa_mask);
    sigint_handler.sa_flags = 0;
    sigaction(SIGINT, &sigint_handler, NULL);
}

void green_color(float* out) {
    auto rgbf_out = (rgbf*) out;
    *rgbf_out = {0.0f, 0.9f, 0.3f};
}

void black_color(float* out) {
    auto rgbf_out = (rgbf*) out;
    *rgbf_out = {0.05f, 0.05f, 0.05f};
}

void white_color(float* out) {
    auto rgbf_out = (rgbf*) out;
    *rgbf_out = {1.0f, 1.0f, 1.0f};
}

void display() {
    float* pixels = new float[SIZE*3];
    while (1) {
        if (CTRLC) {
            std::cout << "Ctrl-C caught, saving to: " << world.data_path << std::endl;
            world.pb_write();
            std::cout << "Saved!" << std::endl;
            exit(0);
        }
        for(int i=0; i<SIZE; i++) {
            black_color(&pixels[i*3]);
        } 
        world.draw_foods(pixels);
        world.draw_critters(pixels);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glDrawPixels(WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGB, GL_FLOAT, pixels);
        glutSwapBuffers();
        world.tick();
        if (world.num_alive() < constants::MIN_LIFE) {
            world.reset_critters();
        }
        if (world.food_alive() < constants::MIN_FOOD) {
            world.reset_foods();
        }
        //world.debug();
        std::cout << world.num_alive() << " left\n";
        std::cout << i << " tick!\n";
        i++;
    }
}

void load_world(std::string data_path) {
    struct stat sb;
    stat(data_path.c_str(), &sb);
    world.data_path = data_path;
    if (S_ISDIR(sb.st_mode)) {
        std::cout << "Loading " << data_path << std::endl;
        world.pb_read(data_path);
    }
}

int main(int argc, char* argv[]) {
    int opt;
    static struct option long_options[] = {
        {"dir", required_argument, 0, 'd'},
        {"help", no_argument, 0, 'h'},
    };
    int option_index = 0;
    std::string data_path = "default_world";
    while ((opt = getopt_long(argc, argv, "hd:", long_options, &option_index)) != -1) {
        switch (opt) {
            case 'd':
                data_path = std::string(optarg);
                break;
            case 'h':
                std::cerr << "Usage: brainlife [--dir|-d WORLD_DIR] [--help|-h]" << std::endl;
                exit(0);
            case '?':
                break;
            default:
                std::cerr << "Usage: brainlife [--dir|-d WORLD_DIR] [--help|-h]" << std::endl;
                exit(1);
        }
    }
    load_world(data_path);
    setup_sigint();
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    glutCreateWindow("brainlife display");
    glutDisplayFunc(display);
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glutMainLoop();
}
