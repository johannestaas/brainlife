#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <memory>
#include <utility>
#include <vector>
#include <string>
#include <map>
#include <sys/types.h>
#include <sys/stat.h>

#include "brainlife.pb.h"
#include "brain/random.h"
#include "constants.h"
#include "world.h"

World::World(int w, int h) {
    width = w;
    height = h;
    setup_rng(constants::STDDEV);
    critters.reserve(100000);
    foods.reserve(100000);
    reset_critters();
}

CritterP World::new_critter() {
    auto c = std::make_shared<Critter>(this);
    c->set_pos(random_start());
    critters.push_back(c);
    critter_map[c->get_pos()] = c;
    return c;
}

FoodP World::new_food() {
    auto u = rand_int(3);
    Pos p;
    if ((u > 0) || (int(foods.size()) == 0)) {
        p = random_start();
    } else {
        auto u2 = rand_int(int(foods.size()));
        auto uf = foods.at(u2);
        p = random_adj(uf->x, uf->y);
    }
    auto f = std::make_shared<Food>(p.first, p.second, constants::FOOD_ENERGY_INIT);
    foods.push_back(f);
    food_map[f->get_pos()] = f;
    return f;
}

void World::reset_critters() {
    int crit_amt = constants::CRITTER_INIT - int(critters.size());
    for (int i=0; i<crit_amt; i++) {
        new_critter();
    }
    reset_foods();
}

void World::reset_foods() {
    int regen_amt = constants::FOOD_INIT - int(foods.size());
    for (int j=0; j<regen_amt; j++) {
        new_food();
    }
}

float* World::get_pixel(float *pixels, int x, int y) {
    int row = y * width * 3;
    int col = x * 3;
    return &pixels[row + col];
}

void World::draw_foods(float *pixels) {
    for (auto &f : foods) {
        rgbf *p = (rgbf*)get_pixel(pixels, f->x, f->y);
        float aqua = (f->energy / float(constants::FOOD_MAX) * 0.5f) + 0.5f;
        *p = {0.0f, aqua, aqua};
    }
}

void World::draw_critters(float *pixels) {
    for (auto &c : critters) {
        rgbf *p = (rgbf*)get_pixel(pixels, c->x, c->y);
        float green = float(c->energy) / float(constants::MAX_ENERGY);
        *p  = {1.0f - green, green, 0.0f};
    }
}

int World::rand_int(int mx) {
    // 0 to 1
    double r = (rng.uniform() + 1.0) / 2.0;
    return int(double(mx) * r);
}

Pos World::random_start() {
    while (1) {
        int x = rand_int(width);
        int y = rand_int(height);
        auto rp = Pos({x, y});
        auto crit_it = critter_map.find(rp);
        // if nothing there...
        if (crit_it == critter_map.end()) {
            auto food_it = food_map.find(rp);
            // found a spot
            if (food_it == food_map.end()) {
                return rp;
            }
        }
    }
}

Pos World::fix_pos(Pos p) {
    int x = p.first;
    int y = p.second;
    if (x >= width) {
        x = x - width;
    } else if (x < 0) {
        x = width + x;
    }
    if (y >= height) {
        y = y - height;
    } else if (y < 0) {
        y = height + y;
    }
    return Pos({x, y});
}

Pos World::random_adj(int ox, int oy, int d) {
    while (1) {
        for (int i=0; i < 2 * d * d; i++) {
            int x = rand_int(d + 1 + d) + ox - d;
            int y = rand_int(d + 1 + d) + oy - d;
            auto rp = fix_pos(x, y);
            if (rp.first == ox && rp.second == oy) {
                continue;
            }
            auto crit_it = critter_map.find(rp);
            // if nothing there...
            if (crit_it == critter_map.end()) {
                auto food_it = food_map.find(rp);
                // found a spot
                if (food_it == food_map.end()) {
                    return rp;
                }
            }
        }
        d++;
    }
    // return Pos({-1, -1});
}

void World::tick() {
    for (auto &cu : critters) {
        cu->move();
    }

    FoodV::iterator food_it = foods.begin();
    while (food_it != foods.end()) {
        if ((*food_it)->is_gone()) {
            food_map.erase((*food_it)->get_pos());
            food_it = foods.erase(food_it);
        } else {
            food_it++;
        }
    }

    CritterV::iterator critter_it = critters.begin();
    while (critter_it != critters.end()) {
        if (((*critter_it)->energy <= 0) || ((*critter_it)->laziness > constants::MAX_LAZINESS) || ((*critter_it)->age >= constants::MAX_AGE)) {
            critter_map.erase((*critter_it)->get_pos());
            critter_it = critters.erase(critter_it);
        } else {
            critter_it++;
        }
    }

    for (int i=0; i<constants::FOOD_PER_TICK; i++) {
        if (int(foods.size()) >= constants::FOOD_MAX_GROWTH) {
            break;
        }
        new_food();
    }
}

Pos World::stop_wall(Pos pos, int dx, int dy) {
    int new_x = pos.first + dx;
    int new_y = pos.second + dy;
    return fix_pos(new_x, new_y);
}

bool World::move(Critter *c, int dx, int dy) {
    auto pos = c->get_pos();
    auto next = stop_wall(pos, dx, dy);
    if (pos == next) {
        return false;
    }
    auto other_crit_it = critter_map.find(next);
    // Nothing there, move critter.
    if (other_crit_it == critter_map.end()) {
        c->set_pos(next);
        critter_map[next] = critter_map[pos];
        critter_map.erase(pos);   
        return true;
    }
    // Otherwise, do nothing. Maybe substitute for fight?
    return false;
}

bool World::eat(Critter *c) {
    auto pos = c->get_pos();
    auto food_it = food_map.find(pos);
    // Food exists, eat it.
    if (food_it != food_map.end()) {
        auto food = food_it->second;
        int amt_eaten = food->eat(constants::FOOD_EAT);
        c->add_energy(amt_eaten);
        return true;
        // Handle removing food afterwards
    }
    return false;
    // Otherwise, do nothing. No food for critter.
}

bool World::split(Critter *c) {
    int new_energy = c->split();
    if (new_energy <= 0) {
        // not enough energy to split
        return false;
    }
    int old_x = c->x;
    int old_y = c->y;
    Pos p = random_adj(c->x, c->y);
    if (!move(c, p.first, p.second)) {
        return false;
    }
    //std::cout << "x: " << p.first << " y: " << p.second << std::endl;
    auto baby = std::make_shared<Critter>(*c);
    baby->x = old_x;
    baby->y = old_y;
    baby->energy = new_energy;
    baby->dx = c->dx;
    baby->dy = c->dy;
    baby->mutate();
    baby->age = 0;
    critters.push_back(baby);
    critter_map[baby->get_pos()] = baby;
    return true;
}   

bool World::fight(Critter *c) {
    return false;
}

double World::look_food_west(Critter *c) {
    auto pos = c->get_pos();
    for (int d=-1; d>-4; d--) {
        auto target = stop_wall(pos, d, 0);
        if (target.first == pos.first && target.second == pos.second) {
            return -1.0;
        }
        auto food_it = food_map.find(target);
        if (food_it != food_map.end()) {
            return double(abs(d));
        }
    }
    return -1.0;
}

double World::look_food_north(Critter *c) {
    auto pos = c->get_pos();
    for (int d=-1; d>-4; d--) {
        auto target = stop_wall(pos, 0, d);
        if (target.first == pos.first && target.second == pos.second) {
            return -1.0;
        }
        auto food_it = food_map.find(target);
        if (food_it != food_map.end()) {
            return double(abs(d));
        }
    }
    return -1.0;
}

double World::look_food_east(Critter *c) {
    auto pos = c->get_pos();
    for (int d=1; d<4; d++) {
        auto target = stop_wall(pos, d, 0);
        if (target.first == pos.first && target.second == pos.second) {
            return -1.0;
        }
        auto food_it = food_map.find(target);
        if (food_it != food_map.end()) {
            return double(abs(d));
        }
    }
    return -1.0;
}

double World::look_food_south(Critter *c) {
    auto pos = c->get_pos();
    for (int d=1; d<4; d++) {
        auto target = stop_wall(pos, 0, d);
        if (target.first == pos.first && target.second == pos.second) {
            return -1.0;
        }
        auto food_it = food_map.find(target);
        if (food_it != food_map.end()) {
            return double(abs(d));
        }
    }
    return -1.0;
}

double World::look_enemy_west(Critter *c) {
    auto pos = c->get_pos();
    for (int d=-1; d>-4; d--) {
        auto target = stop_wall(pos, d, 0);
        if (target.first == pos.first && target.second == pos.second) {
            return -1.0;
        }
        auto critter_it = critter_map.find(target);
        if (critter_it != critter_map.end()) {
            return double(abs(d));
        }
    }
    return -1.0;
}

double World::look_enemy_north(Critter *c) {
    auto pos = c->get_pos();
    for (int d=-1; d>-4; d--) {
        auto target = stop_wall(pos, 0, d);
        if (target.first == pos.first && target.second == pos.second) {
            return -1.0;
        }
        auto critter_it = critter_map.find(target);
        if (critter_it != critter_map.end()) {
            return double(abs(d));
        }
    }
    return -1.0;
}

double World::look_enemy_east(Critter *c) {
    auto pos = c->get_pos();
    for (int d=1; d<4; d++) {
        auto target = stop_wall(pos, d, 0);
        if (target.first == pos.first && target.second == pos.second) {
            return -1.0;
        }
        auto critter_it = critter_map.find(target);
        if (critter_it != critter_map.end()) {
            return double(abs(d));
        }
    }
    return -1.0;
}

double World::look_enemy_south(Critter *c) {
    auto pos = c->get_pos();
    for (int d=1; d<4; d++) {
        auto target = stop_wall(pos, 0, d);
        if (target.first == pos.first && target.second == pos.second) {
            return -1.0;
        }
        auto critter_it = critter_map.find(target);
        if (critter_it != critter_map.end()) {
            return double(abs(d));
        }
    }
    return -1.0;
}

void World::setup_rng(double stddev) {
    rng = RandomNumberGenerator();
    rng.setup_uniform_dist(-1.0, 1.0);
    rng.setup_normal_dist(0.0, stddev);
}

void World::debug() {
    for (auto &c : critters) {
        c->debug();
    }
}

void World::pb_write() {
    pb_write(data_path);
}

void World::pb_write(std::string dirpath) {
    if (dirpath.empty()) {
        std::cerr << "Tried to call pb_write with empty dirpath." << std::endl;
        throw NULL;
    }
    struct stat sb;
    stat(dirpath.c_str(), &sb);
    if (S_ISREG(sb.st_mode)) {
        std::cerr << dirpath << " is a file, need a directory name." << std::endl;
    } else if (!S_ISDIR(sb.st_mode)) {
        mkdir(dirpath.c_str(), S_IRWXU);
        std::cout << "Created directory " << dirpath << std::endl;
    }
    for (int i=0; i<int(critters.size()); i++) {
        std::stringstream ss;
        ss << dirpath << "/crit_" << std::setw(6) << std::setfill('0') << i << ".bin";
        std::ofstream output_f;
        output_f.open(ss.str(), std::ios::out | std::ios::binary);
        brainlife::Critter crit;
        critters.at(i)->pb_serialize(&crit);
        if (!crit.SerializeToOstream(&output_f)) {
            std::cerr << "Failed to save critter " << i << " to " << ss.str() << std::endl;
            critters.at(i)->debug();
            throw NULL;
            //std::cerr << "Skipping..." << std::endl;
            //i--;
        }
    }
    for (int i=0; i<int(foods.size()); i++) {
        std::stringstream ss;
        ss << dirpath << "/food_" << std::setw(6) << std::setfill('0') << i << ".bin";
        std::ofstream output_f;
        output_f.open(ss.str(), std::ios::out | std::ios::binary);
        brainlife::Food food;
        foods.at(i)->pb_serialize(&food);
        if (!food.SerializeToOstream(&output_f)) {
            std::cerr << "Failed to save food " << i << " to " << ss.str() << std::endl;
            foods.at(i)->debug();
            throw NULL;
        }
    }
    std::stringstream ss;
    ss << dirpath << "/world.bin";
    std::ofstream output_f;
    output_f.open(ss.str(), std::ios::out | std::ios::binary);
    brainlife::World world;
    world.set_width(width);
    world.set_height(height);
    world.set_stddev(rng.stddev);
    world.set_critters_size(int(critters.size()));
    world.set_foods_size(int(foods.size()));
    if (!world.SerializeToOstream(&output_f)) {
        std::cerr << "Failed to save world to " << ss.str() << std::endl;
        throw NULL;
    }
} 

void World::pb_read() {
    pb_read(data_path);
}

void World::pb_read(std::string dirpath) {
    data_path = dirpath;
    std::stringstream ss;
    ss << dirpath << "/world.bin";
    std::ifstream input_f;
    input_f.open(ss.str(), std::ios::in | std::ios::binary);
    brainlife::World pb_world;
    if (!pb_world.ParseFromIstream(&input_f)) {
        std::cerr << "Failed to load world from " << ss.str() << std::endl;
        throw NULL;
    }
    width = pb_world.width();
    height = pb_world.height();
    setup_rng(pb_world.stddev());
    critters.clear();
    foods.clear();
    for (int i=0; i<pb_world.critters_size(); i++) {
        std::stringstream ss;
        ss << dirpath << "/crit_" << std::setw(6) << std::setfill('0') << i << ".bin";
        std::ifstream input_f;
        input_f.open(ss.str(), std::ios::in | std::ios::binary);
        if (!input_f) {
            std::cout << "File not found: " << ss.str() << std::endl;
            throw NULL;
        }
        brainlife::Critter pb_crit;
        if (!pb_crit.ParseFromIstream(&input_f)) {
            std::cout << "Failed to parse critter protobof: " << ss.str() << std::endl;
            throw NULL;
        }
        auto c = std::make_shared<Critter>(pb_crit);
        c->brain->rng = &rng;
        c->world = this;
        critters.push_back(c);
        critter_map[c->get_pos()] = c;
    }
    for (int i=0; i<pb_world.foods_size(); i++) {
        std::stringstream ss;
        ss << dirpath << "/food_" << std::setw(6) << std::setfill('0') << i << ".bin";
        std::ifstream input_f;
        input_f.open(ss.str(), std::ios::in | std::ios::binary);
        if (!input_f) {
            std::cout << "File not found: " << ss.str() << std::endl;
            throw NULL;
        }
        brainlife::Food pb_food;
        if (!pb_food.ParseFromIstream(&input_f)) {
            std::cout << "Failed to parse food protobof: " << ss.str() << std::endl;
            throw NULL;
        }
        auto f = std::make_shared<Food>(pb_food);
        foods.push_back(f);
        food_map[f->get_pos()] = f;
    }
}

World& World::operator=(const brainlife::World &pb_world) {
    width = pb_world.width();
    height = pb_world.height();
    setup_rng(pb_world.stddev());
    critters.clear();
    foods.clear();
    return *this;
}

World::World(const brainlife::World& pb_world) {
    *this = pb_world;
}
