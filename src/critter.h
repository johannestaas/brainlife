#pragma once
#include <iostream>
#include <memory>
#include <vector>
#include <cmath>
#include "brainlife.pb.h"
#include "brain/neural_net.h"
#include "constants.h"
#include "world.h"

class World;

struct Mem {
    int i;
    double mem[16];
};

class Critter {
    public:
        Critter(World*);
        Critter(const Critter&);
        Critter(const brainlife::Critter&);
        Critter() = default;
        ~Critter() = default;
        Critter& operator=(const Critter&) = default;
        Critter& operator=(const brainlife::Critter&);
        void move();
        void think();
        void mutate();
        std::vector<double> brain_inputs();
        void process_output(std::vector<double>);
        void add_energy(int);
        int split();

        Pos get_pos() const { return Pos({x, y}); };
        void set_pos(int xx, int yy) { x = xx; y = yy; };
        void set_pos(Pos p) { x = p.first; y = p.second; };
        void debug();
        void pb_serialize(brainlife::Critter*);
        std::shared_ptr<FFNet> brain;
        World* world;
        int energy;
        int max_energy;
        int x, y;
        int dx, dy;
        int laziness;
        int age;
        bool try_eat, try_split, try_fight;
        Mem mem;
        int mem_index;
};

typedef std::shared_ptr<Critter> CritterP;
