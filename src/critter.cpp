#include <memory>
#include <vector>
#include <utility>
#include "brainlife.pb.h"
#include "constants.h"
#include "critter.h"
#include "world.h"

class World;

Critter::Critter(World *the_world) {
    brain = std::make_shared<FFNet>(the_world->get_rng());
    brain->add_input_layer(12);
    brain->add_hidden_layers(constants::HIDDEN_WIDTH, constants::HIDDEN_LAYERS);
    brain->add_output_layer(7);
    world = the_world;
    max_energy = constants::MAX_ENERGY;
    energy = constants::START_ENERGY;
    x = 0;
    y = 0;
    age = 0;
    laziness = 0;
    mem.i = 0;
    for (int i=0; i<16; i++) {
        mem.mem[i] = 0;
    }
    mem_index = 0;
}

Critter::Critter(const Critter& dad) {
    world = dad.world;
    brain = std::make_shared<FFNet>(*(dad.brain));
    max_energy = constants::MAX_ENERGY;
    energy = constants::START_ENERGY;
    x = dad.x;
    y = dad.y;
    age = 0;
    laziness = 0;
    mem.i = dad.mem.i;
    for (int i=0; i<16; i++) {
        mem.mem[i] = dad.mem.mem[i];
    }
    mem_index = 0;
}

void Critter::move() {
    think();
    int cost = ceil(sqrt((dx * dx) + (dy * dy)) * constants::MOVE_COST);
    if (energy >= cost && world->move(this, dx, dy)) {
        energy -= cost;
        laziness = 0;
    }
    if (try_eat) {
        if (world->eat(this)) {
            laziness = 0;
        }
    }
    if (try_split) {
        if (world->split(this)) {
            laziness = -50;
        }
    }
    /*if (try_fight) {
        world->fight(this);
    }*/
    // Decrement energy per tick
    energy -= 2;
    laziness++;
    age++;
}

void Critter::mutate() {
    brain->mutate();
}

void Critter::think() {
    brain->reset();
    std::vector<double> inputs = brain_inputs();
    brain->activate(&inputs);
    std::vector<double> outputs = brain->get_output();
    process_output(outputs);
}

std::vector<double> Critter::brain_inputs() {
    std::vector<double> dv = {
        //double(x),
        //double(y),
        double(energy),
        //double(max_energy),
        double(age),
        double(laziness),
        //double(dx),
        //double(dy),
        //double(try_eat),
        //double(try_split),
        //double(try_fight),
        world->look_food_west(this),
        world->look_food_north(this),
        world->look_food_east(this),
        world->look_food_south(this),
        world->look_enemy_west(this),
        world->look_enemy_north(this),
        world->look_enemy_east(this),
        world->look_enemy_south(this),
        mem.mem[mem.i],
    };
    return dv;
}

void Critter::process_output(std::vector<double> outputs) {
    dx = int(outputs.at(0));
    dy = int(outputs.at(1));
    try_eat = (outputs.at(2) >= 0.0);
    try_split = (outputs.at(3) >= 0.0);
    try_fight = (outputs.at(4) >= 0.0);
    mem.mem[mem.i] = outputs.at(5);
    mem.i = int(outputs.at(6) * 16.0);
    mem.i = mem.i > 15 || mem.i < 0 ? 0 : mem.i;
}

void Critter::add_energy(int e) {
    energy += e;
    energy = (energy > max_energy) ? max_energy : energy;
}

int Critter::split() {
    if (energy > constants::MIN_SPLIT) {
        energy /= 2;
        return energy;
    } else {
        return 0;
    }
}

void Critter::debug() {
    std::cout << "{x: " << x << ", y: " << y << ", e: " << energy << ", me: " << max_energy << ", dx: " << dx << ", dy: " << dy << ", laziness: " << laziness << ", age: " << age << ", try_eat: " << try_eat << ", try_split: " << try_split << ", try_fight: " << try_fight << ", mem_index: " << mem_index << ", mem.i: " << mem.i << "}" << std::endl;
}

void Critter::pb_serialize(brainlife::Critter *pb_crit) {
    pb_crit->set_energy(energy);
    pb_crit->set_max_energy(max_energy);
    if (x < 0 || y < 0) {
        std::cerr << "Issue saving x,y of critter: " << x << "," << y << std::endl;
    }
    pb_crit->set_x(x);
    pb_crit->set_y(y);
    pb_crit->set_dx(dx);
    pb_crit->set_dy(dy);
    pb_crit->set_laziness(laziness);
    pb_crit->set_age(age);
    pb_crit->set_try_eat(try_eat);
    pb_crit->set_try_split(try_split);
    pb_crit->set_try_fight(try_fight);
    pb_crit->set_mem_index(mem_index);
    cbrain::FeedForwardNeuralNet *pb_brain = pb_crit->mutable_brain();
    brain->pb_serialize(pb_brain);   
    brainlife::Memory *pb_mem = pb_crit->mutable_mem();
    pb_mem->set_i(mem.i);
    pb_mem->mutable_mem()->Reserve(16);
    for (int i=0; i<16; i++) {
        pb_mem->add_mem(mem.mem[i]);
    }
} 

Critter& Critter::operator=(const brainlife::Critter &pb_crit) {
    energy = pb_crit.energy();
    max_energy = pb_crit.max_energy();
    x = pb_crit.x();
    y = pb_crit.y();
    dx = pb_crit.dx();
    dy = pb_crit.dy();
    laziness = pb_crit.laziness();
    age = pb_crit.age();
    try_eat = pb_crit.try_eat();
    try_split = pb_crit.try_split();
    try_fight = pb_crit.try_fight();
    mem_index = pb_crit.mem_index();
    brain = std::make_shared<FFNet>(pb_crit.brain());
    mem.i = pb_crit.mem().i();
    for (int i=0; i<pb_crit.mem().mem_size(); i++) {
        mem.mem[i] = pb_crit.mem().mem(i);
    }
    return *this;
}

Critter::Critter(const brainlife::Critter &pb_crit) {
    *this = pb_crit;
}

