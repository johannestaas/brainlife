project(BRAINLIFE)
set(CFILES brainlife.cpp world.cpp critter.cpp food.cpp brainlife.pb.cc)
set(HFILES brainlife.h world.h critter.h food.h constants.h brainlife.pb.h)
add_executable(brainlife ${CFILES} ${HFILES})
find_library(BRAIN
    NAMES brain
    PATHS /usr/local/lib
)
find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)
include_directories(${OPENGL_INCLUDE_DIRS}  ${GLUT_INCLUDE_DIRS})
target_link_libraries(brainlife ${BRAIN} ${OPENGL_LIBRARIES} ${GLUT_LIBRARY} "-lprotobuf")
install(TARGETS brainlife RUNTIME DESTINATION bin LIBRARY DESTINATION lib)
