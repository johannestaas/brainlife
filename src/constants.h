#pragma once
#include <utility>

typedef std::pair<int, int> Pos;

struct rgbf {float r; float g; float b;};

namespace constants {
    const double STDDEV = 0.04;
    const int MIN_LIFE = 5;
    const int MIN_FOOD = 1;
    const int FOOD_PER_TICK = 10;
    const int HIDDEN_WIDTH = 12;
    const int HIDDEN_LAYERS = 3;
    const int MAX_LAZINESS = 100;
    const int MAX_AGE = 2500;
    const int CRITTER_INIT = 15000;
    const int FOOD_ENERGY_INIT = 2000;
    const int FOOD_INIT = 20000;
    const int FOOD_MAX_GROWTH = 20000;
    const int FOOD_EAT = 2000;
    const int FOOD_MAX = 2000;
    const int FOOD_REGEN = 5;
    const int MIN_SPLIT = 5000;
    const int START_ENERGY = 5000;
    const int MAX_ENERGY = 20000;
    const int MOVE_COST = 10;
}
