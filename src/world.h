#pragma once
#include <map>
#include <iostream>
#include <vector>
#include <utility>
#include <memory>
#include "brainlife.pb.h"
#include "brain/random.h"
#include "constants.h"
#include "critter.h"
#include "food.h"

class Critter;
class Food;

typedef std::vector<std::shared_ptr<Critter>> CritterV;
typedef std::vector<std::shared_ptr<Food>> FoodV;
typedef std::map<Pos, std::shared_ptr<Critter>> CritterMap;
typedef std::map<Pos, std::shared_ptr<Food>> FoodMap;

class World {
    public:
        World(int, int);
        World(const brainlife::World&);
        World() = default;
        ~World() = default;
        World(const World&) = default;
        World& operator=(const World&) = default;
        World& operator=(const brainlife::World&);
        std::shared_ptr<Critter> new_critter();
        std::shared_ptr<Food> new_food();
        float* get_pixel(float*, int, int);
        void draw_critters(float*);
        void draw_foods(float*);
        Pos stop_wall(Pos, int, int);
        Pos random_start();
        Pos random_adj(int, int, int);
        Pos random_adj(int ox, int oy) { return random_adj(ox, oy, 1); };
        Pos fix_pos(Pos);
        Pos fix_pos(int x, int y) { return fix_pos(Pos({x, y})); };
        int rand_int(int);
        bool move(Critter*, int, int);
        bool eat(Critter*);
        bool split(Critter*);
        bool fight(Critter*);
        bool is_dead() { return (int)critters.size() == 0; };
        void tick();
        void reset_foods();
        void reset_critters();
        double look_food_west(Critter*);
        double look_food_north(Critter*);
        double look_food_east(Critter*);
        double look_food_south(Critter*);
        double look_enemy_west(Critter*);
        double look_enemy_north(Critter*);
        double look_enemy_east(Critter*);
        double look_enemy_south(Critter*);
        RandomNumberGenerator* get_rng() { return &rng; };
        void setup_rng(double);
        CritterV* get_critters() { return &critters; };
        FoodV* get_foods() { return &foods; };
        void debug();
        int num_alive() { return (int) critters.size(); };
        int food_alive() { return (int) foods.size(); };
        void pb_write(std::string);
        void pb_write();
        void pb_read(std::string);
        void pb_read();
        std::string data_path;
    private:
        RandomNumberGenerator rng;
        CritterV critters;
        FoodV foods;
        CritterMap critter_map;
        FoodMap food_map;
        int width;
        int height;
};
